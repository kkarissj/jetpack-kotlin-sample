package park.jbtm.jetpacksample.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "times")
data class TimeEntity(
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0,
        var time: Long)