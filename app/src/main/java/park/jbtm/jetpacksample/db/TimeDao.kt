package park.jbtm.jetpacksample.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface TimeDao {

    @Query("SELECT * FROM times")
    fun getAllTimes(): List<TimeEntity>

    @Insert
    fun insertTime(time: TimeEntity)
}