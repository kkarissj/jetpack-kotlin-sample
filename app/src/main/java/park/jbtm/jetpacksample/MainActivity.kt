package park.jbtm.jetpacksample

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.persistence.room.Room
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import park.jbtm.jetpacksample.db.TimeDatabase
import park.jbtm.jetpacksample.db.TimeEntity

class MainActivity : AppCompatActivity() {

    lateinit var db: TimeDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        db = Room.databaseBuilder(this, TimeDatabase::class.java, "time_db").allowMainThreadQueries().build()
        val timeList = db.timeDao().getAllTimes()

        if (timeList.isNotEmpty()) {
            timeList.forEach {
                Log.d("MainActivity", "time value is ${it.time}")
            }
        }

        testChronometer()
        testLiveData()

        locationButton.setOnClickListener {
            startActivity(Intent(this, LocationActivity::class.java))
        }


    }

    private fun testChronometer() {
        val chronometerViewModel: ChronometerViewModel =
                ViewModelProviders.of(this)[ChronometerViewModel::class.java]

        if (chronometerViewModel.startTime == 0L) {
            val startTime = SystemClock.elapsedRealtime()
            chronometerViewModel.startTime = startTime
            chronometer.base = startTime
        } else {
            chronometer.base = chronometerViewModel.startTime
        }

        chronometer.start()
    }

    private fun testLiveData() {
        val liveDataTimerViewModel: LiveDataTimerViewModel =
                ViewModelProviders.of(this)[LiveDataTimerViewModel::class.java]

        liveDataTimerViewModel.elapsedTime.observe(this, Observer<Long> { t ->
            val newText = "$t seconds elapsed"
            timerText.text = newText
            db.timeDao().insertTime(TimeEntity(time = t!!))
        })
    }
}
