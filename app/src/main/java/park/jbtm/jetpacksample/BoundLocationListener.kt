package park.jbtm.jetpacksample

import android.annotation.SuppressLint
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.location.LocationListener
import android.location.LocationManager
import android.util.Log

class BoundLocationManager {

    companion object {
        fun bindLocationListenerIn(lifecycleOwner: LifecycleOwner,
                                   listener: LocationListener, context: Context) {
            BoundLocationListener(lifecycleOwner, listener, context)
        }
    }

    @SuppressLint("MissingPermission")
    class BoundLocationListener(lifecycleOwner: LifecycleOwner,
                                private val listener: LocationListener, private val context: Context) : LifecycleObserver {
        private var locationManager: LocationManager? = null

        init {
            lifecycleOwner.lifecycle.addObserver(this)
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        fun addLocationListener() {
            locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            locationManager?.let {
                it.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 0F, listener)
                Log.d("BoundLocationMgr", "Listener Added")
                it.getLastKnownLocation(LocationManager.GPS_PROVIDER)?.let {
                    listener.onLocationChanged(it)
                }
            }

        }

        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        fun removeLocationListener() {
            if (locationManager == null) return

            locationManager!!.removeUpdates(listener)
            locationManager = null
            Log.d("BoundLocationMgr", "Listener removed")

        }
    }
}